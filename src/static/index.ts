const staticUrls = {
  logo: require( './logo.svg' ),
  placeholderIcon: require( './placeholder-icon.svg' ),

  ageNew: require( './age-new.svg' ),
  ageOld: require( './age-old.svg' ),
  arrowDown: require( './arrow-down.svg' ),
  arrowUp: require( './arrow-up.svg' ),
  calendar: require( './calendar.svg' ),
  category: require( './category.svg' ),
  creditCard: require( './credit-card.svg' ),
  home: require( './home.svg' ),
  map: require( './map.svg' ),
  place: require( './place.svg' ),
  radius: require( './radius.svg' ),
  sort: require( './sort.svg' ),
  world: require( './world.svg' ),
};

export default staticUrls;
