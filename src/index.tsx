import * as React from 'react';
import * as ReactDOM from 'react-dom';
import mock from './mock';
import { ThemeProvider } from 'styled-components';
import theme from './common/theme';
import './common/globalStyles';
import App from './components/App';

ReactDOM.render(
  (
    <ThemeProvider theme={ theme }>
      <App page={ mock.page } />
    </ThemeProvider>
  ),
  document.getElementById( 'root' ) as HTMLElement
);
