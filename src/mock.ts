// This is a temporary mock and should actually be replaced by a GraphQL
// request. If this is still here at the presentation, I probably had not
// enough time to implement a relay implementation :D
const mock = {
  page: {
    pageHeader: {
      filter: {
        placeholder: 'Search, filter, ...',
        items: [
          {
            __typename: 'FilterRadioGroup',
            id: 'sortBy',
            label: 'Sort by',
            icon: 'sort',
            defaultValue: 'distance',
            options: [
              {
                value: 'distance',
                label: 'Distance',
              },
              {
                value: 'date',
                label: 'Date',
              },
              {
                value: 'price-increasing',
                label: 'Price',
                suffixIcon: 'arrowUp',
              },
              {
                value: 'price-decreasing',
                label: 'Price',
                suffixIcon: 'arrowDown',
              },
            ],
          },
          {
            __typename: 'FilterRange',
            id: 'dateRange',
            label: 'Listed in the last',
            icon: 'calendar',
            startIcon: 'ageNew',
            endIcon: 'ageOld',
            defaultValue: 'forever',
            steps: [
              {
                value: '24h',
                label: '24 hours',
              },
              {
                value: '3d',
                label: '3 days',
              },
              {
                value: '7d',
                label: '7 days',
              },
              {
                value: '30d',
                label: '30 days',
              },
              {
                value: 'forever',
                label: 'forever',
              },
            ],
          },
          {
            __typename: 'FilterRange',
            id: 'radius',
            label: 'Radius',
            icon: 'radius',
            startIcon: 'home',
            endIcon: 'world',
            defaultValue: -1,
            steps: [
              {
                value: 1,
                label: '1 km',
              },
              {
                value: 2,
                label: '2 km',
              },
              {
                value: 3,
                label: '3 km',
              },
              {
                value: 4,
                label: '4 km',
              },
              {
                value: 5,
                label: '5 km',
              },
              {
                value: 7,
                label: '7 km',
              },
              {
                value: 10,
                label: '10 km',
              },
              {
                value: 15,
                label: '15 km',
              },
              {
                value: 20,
                label: '20 km',
              },
              {
                value: 30,
                label: '30 km',
              },
              {
                value: 60,
                label: '60 km',
              },
              {
                value: 100,
                label: '100 km',
              },
              {
                value: 200,
                label: '200 km',
              },
              {
                value: 300,
                label: '300 km',
              },
              {
                value: 400,
                label: '400 km',
              },
              {
                value: 500,
                label: '500 km',
              },
              {
                value: 1000,
                label: '1000 km',
              },
              {
                value: -1,
                label: 'Everywhere',
              },
            ],
          },
          {
            __typename: 'FilterCheckbox',
            id: 'onlyInMyCountry',
            label: 'Search only in my country',
            icon: 'map',
            defaultValue: false,
          },
          {
            __typename: 'FilterGeolocation',
            id: 'location',
            label: 'Current location',
            icon: 'place',
            defaultValue: true,
          },
          {
            __typename: 'FilterCategories',
            id: 'categories',
            label: 'Current location',
            icon: 'category',
            defaultValue: [ 'all' ],
            options: [
              {
                value: 'all',
                label: 'All',
                isSolitary: true,
              },
              {
                value: 'newInYourArea',
                label: 'New in your area',
              },
              {
                value: 'fashionAndAccessories',
                label: 'Fashion and Accessories',
              },
              {
                value: 'homeAndGarden',
                label: 'Home and Garden',
              },
              {
                value: 'electronics',
                label: 'Electronics',
              },
              {
                value: 'babyAndChild',
                label: 'Baby and Child',
              },
              {
                value: 'sportLeisureGames',
                label: 'Sport, Leisure and Games',
              },
              {
                value: 'moviesBooksMusic',
                label: 'Movies, Books and Music',
              },
              {
                value: 'carsAndMotors',
                label: 'Cars and Motors',
                isSolitary: true,
              },
              {
                value: 'property',
                label: 'Property',
                isSolitary: true,
              },
              {
                value: 'services',
                label: 'Services',
              },
              {
                value: 'other',
                label: 'Other',
              },
            ],
          },
          {
            __typename: 'FilterMinMax',
            id: 'priceRange',
            label: 'Set your price range',
            icon: 'creditCard',
            defaultValue: [],
            minLabel: 'Min. Price',
            maxLabel: 'Max. Price',
          },
        ],
      },
    },
  },
};

export default mock;
