import { injectGlobal } from 'styled-components';

// tslint:disable-next-line
injectGlobal`
  *, *::before, *::after {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  :root {
    font-size: 62.5%;
  }

  html, body {
    min-height: 100vh;
  }

  body {
    font: 1.4rem/1.25 Helvetica, Arial, sans-serif;
  }
`;
