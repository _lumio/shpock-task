const theme = {
  colors: {
    primary: '#45b862',
    primaryShade: '#7cc450',
    white: '#fff',
    gray1: '#ccc',
    gray2: '#ededed',
    gray3: '#eee',
    gray4: '#f6f6f6',
  },
  media: {
    small: '@media ( max-width: 740px )',
  },
};

export default theme;
