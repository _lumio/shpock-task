type ValueType = string | number | boolean | string[] | number[];
interface ValueCollectionType {
  [ key : string ] : ValueType;
}

export default ValueType;
export { ValueCollectionType };
