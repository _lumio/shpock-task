import styled from 'styled-components';

const PageHeaderStyles = styled.header`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 1rem;

  background:
    linear-gradient(
      to right,
      ${ ( props ) => props.theme.colors.primary } 0%,
      ${ ( props ) => props.theme.colors.primaryShade } 100%
    );

  > img {
    height: 8rem;
    width: auto;
  }
`;

export { PageHeaderStyles };
