interface PageHeaderPropsType {
  pageHeader : any;
  onFilterChange?( values : { [ key : string ] : string | number } ) : void;
}

export { PageHeaderPropsType };
