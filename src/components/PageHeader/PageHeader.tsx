import * as React from 'react';
import Filter from '../Filter';
import { PageHeaderStyles } from './styles';
import staticUrls from '../../static';
import { PageHeaderPropsType } from './types';

const PageHeader : React.StatelessComponent<PageHeaderPropsType> = ( props : PageHeaderPropsType ) => {
  return (
    <PageHeaderStyles>
      <img src={ staticUrls.logo } />
      <Filter filter={ props.pageHeader.filter } onChange={ props.onFilterChange } />
    </PageHeaderStyles>
  );
};

export default PageHeader;
