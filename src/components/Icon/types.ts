interface IconPropsType {
  icon : string;
  className? : string;
}

export { IconPropsType };
