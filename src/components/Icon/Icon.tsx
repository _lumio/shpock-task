import * as React from 'react';
import styled from 'styled-components';
import { IconPropsType } from './types';
import staticUrls from '../../static';

const IconStyles = styled.img`
  height: 1em;
  width: auto;
`;

const Icon : React.StatelessComponent<IconPropsType> = ( props : IconPropsType ) => (
  <IconStyles src={ staticUrls[ props.icon ] } className={ props.className } />
);

export default Icon;
