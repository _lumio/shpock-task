import styled from 'styled-components';

const FilterLabelStyles = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;

  width: 100%;
  font-weight: bold;
  margin-bottom: .5rem;

  & > img {
    margin-right: .5rem;
  }

  .filter-label__label,
  .filter-label__value {
    width: 100%;
  }

  .filter-label__value {
    text-align: right;
  }
`;

export { FilterLabelStyles };
