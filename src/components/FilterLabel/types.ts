interface FilterLabelPropsType {
  icon : string;
  children? : any;
  value? : string;
}

export { FilterLabelPropsType };
