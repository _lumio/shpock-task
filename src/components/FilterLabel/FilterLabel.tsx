import * as React from 'react';
import { FilterLabelPropsType } from './types';
import { FilterLabelStyles } from './styles';
import Icon from '../Icon';

const FilterLabel : React.StatelessComponent<FilterLabelPropsType> = ( props : FilterLabelPropsType ) => {
  return (
    <FilterLabelStyles>
      <Icon icon={ props.icon } />
      <span className='filter-label__label'>{ props.children }</span>
      { props.value ? (
        <span className='filter-label__value'>{ props.value }</span>
      ) : null }
    </FilterLabelStyles>
  );
};

export default FilterLabel;
