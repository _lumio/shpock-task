import styled from 'styled-components';
import { ButtonPropsType } from './types';
import theme from '../../common/theme';

const Button = styled.button`
  ${ ( props : ButtonPropsType ) => '' }
  font-size: 1.4rem;
  padding: .75rem 1rem;

  border: 1px solid ${ theme.colors.primary };
  border-radius: .4rem;
  background: ${ ( props ) => props.primary ? theme.colors.primary : theme.colors.white };
  color: ${ ( props ) => props.primary ? theme.colors.white : theme.colors.primary };
  cursor: pointer;
`;

export default Button;
