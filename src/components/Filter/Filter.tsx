import * as React from 'react';
import {
  FilterPropsType,
  FilterStateType,
} from './types';
import {
  FilterStyles,
  FilterInputStyles,
  FilterContainerStyles,
} from './styles';
import Button from '../Button';
import ValueType from '../../common/valueType';

import FilterCategories from './FilterCategories';
import FilterCheckbox from './FilterCheckbox';
import FilterGeolocation from './FilterGeolocation';
import FilterRadioGroup from './FilterRadioGroup';
import FilterMinMax from './FilterMinMax';
import FilterRange from './FilterRange';
const filterTypeMap = {
  FilterCategories,
  FilterCheckbox,
  FilterGeolocation,
  FilterRadioGroup,
  FilterMinMax,
  FilterRange,
};

class Filter extends React.Component<FilterPropsType, FilterStateType> {
  searchField : any;

  constructor( props : FilterPropsType ) {
    super( props );
    this.state = {
      filtersAreVisible: false,
      values: this.initializeValues(),
    };
  }

  componentDidMount() {
    // TypeScript doesn't allow onSearch on an input field yet
    // so we just add an event listener.
    this.searchField.addEventListener( 'search', this.onSearch );
  }

  componentWillUnmount() {
    if ( !this.searchField ) {
      return;
    }
    this.searchField.removeEventListener( 'search', this.onSearch );
  }

  initializeValues() {
    const values = {};
    for ( const filterItem of this.props.filter.items ) {
      if ( filterItem.defaultValue !== undefined ) {
        values[ filterItem.id ] = filterItem.defaultValue;
      }
    }

    return values;
  }

  resetFilter = () => {
    const values = {
      ...this.initializeValues(),
      search: this.state.values.search,
    };

    this.setState( {
      filtersAreVisible: false,
      values,
    } );

    if ( this.props.onChange ) {
      this.props.onChange( values );
    }
  }

  changeInputValue = ( evt : any ) => {
    this.changeValue( 'search', evt.target.value );
    this.setState( { filtersAreVisible: !!evt.target.value } );
  }

  onSearch = ( evt : any ) => {
    this.changeInputValue( evt );
    this.resetFilter();
    this.applyFilters();
  }

  onFocus = () => {
    if ( this.state.values.search ) {
      this.setState( { filtersAreVisible: true } );
    }
  }

  onKeyPress = ( evt : any ) => {
    if ( evt.key !== 'Enter' ) {
      return;
    }

    evt.preventDefault();
    if ( !this.state.values.search ) {
      return this.resetFilter();
    }

    this.applyFilters();
  }

  hideFilters = () => {
    this.setState( { filtersAreVisible: false } );
  }

  applyFilters = () => {
    if ( this.props.onChange ) {
      this.props.onChange( this.state.values );
    }
    this.hideFilters();
  }

  changeValue = ( filterName : string, value : ValueType ) => {
    const values = { ...this.state.values };
    values[ filterName ] = value;
    this.setState( { values } );
  }

  renderFilterItems() {
    if ( !this.props.filter || !this.props.filter.items ) {
      return null;
    }

    return this.props.filter.items.map( ( item : any, index : number ) => {
      if ( !item.__typename ) {
        // tslint:disable-next-line
        console.error( 'Item has no typename', item );
        throw new Error( 'No typename for filter item received. See message above (in console)' );
      }

      if ( !filterTypeMap[ item.__typename ] ) {
        throw new Error( `Cannot map filter item type ${ item.__typename }` );
      }

      if ( !item.id ) {
        throw new Error( `Item of type ${ item.__typename } has no ID` );
      }

      const FilterItem = filterTypeMap[ item.__typename ];
      return (
        <FilterItem
          key={ item.id }
          item={ item }
          value={ this.state.values[ item.id ] }
          onChange={ ( value : ValueType ) => (
            this.changeValue( item.id, value )
          ) }
          filtersAreVisible={ this.state.filtersAreVisible }
        />
      );
    } );
  }

  render() {
    const value = this.state.values.search ? String( this.state.values.search ) : '';
    return (
      <FilterStyles>
        <FilterInputStyles
          type='search'
          className={ this.state.filtersAreVisible ? 'filter__input-filters-are-visible' : '' }
          onChange={ this.changeInputValue }
          onFocus={ this.onFocus }
          onKeyPress={ this.onKeyPress }
          value={ value }
          placeholder={ this.props.filter.placeholder }
          innerRef={ ( dom ) => this.searchField = dom }
        />
        <FilterContainerStyles isVisible={ this.state.filtersAreVisible }>
          <div className='filter__container__inner'>
            { this.renderFilterItems() }
          </div>
          <div className='filter__container__action'>
            <Button onClick={ this.applyFilters } primary>Apply</Button>
            <Button onClick={ this.resetFilter }>Reset filter</Button>
          </div>
        </FilterContainerStyles>
      </FilterStyles>
    );
  }
}

export default Filter;
