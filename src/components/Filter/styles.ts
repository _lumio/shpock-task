import styled from 'styled-components';
import theme from '../../common/theme';
import {
  FilterContainerStylesPropsType,
  FilterItemContainer,
} from './types';

const FilterStyles = styled.div`
  position: relative;
  width: 100%;
  max-width: 80rem;
  margin: 0 1rem 0 2rem;
`;

const FilterInputStyles = styled.input`
  width: 100%;
  font-size: 1.4rem;
  padding: .8rem;
  border: none;
  border-radius: .4rem;
  box-shadow: 0 0 2rem rgba( 0, 0, 0, .1 );

  &:focus, &.filter__input-filters-are-visible {
    box-shadow: .5rem .5rem 1rem rgba( 0, 0, 0, .3 );
    transform: translate( -.25rem, -.25rem );
    outline: none;
  }

  transition: transform .5s ease, box-shadow .5s ease;
`;

const FilterContainerStyles = styled.div`
  ${ ( props : FilterContainerStylesPropsType ) => '' }
  display: flex;
  flex-direction: column;
  justify-content: space-between;

  position: absolute;
  left: ${ ( props ) => props.isVisible ? '-.25rem' : 0 };
  top: ${ ( props ) => props.isVisible ? '4rem' : '4.25rem' };
  width: 100%;
  height: ${ ( props ) => props.isVisible ? '45rem' : 0 };
  max-height: 80vh;

  opacity: ${ ( props ) => props.isVisible ? 1 : 0 };
  overflow: hidden;
  padding: .5rem;

  background: #fff;
  border: 1px solid ${ theme.colors.gray3 };
  border-radius: .4rem;
  box-shadow: .5rem .5rem 1rem rgba( 0, 0, 0, .3 );

  transition: left .5s ease, top .5s ease, height .5s ease, opacity .25s;

  .filter__container__inner {
    display: flex;
    justify-content: space-between;
    align-items: flex-start;
    flex-wrap: wrap;

    width: 100%;
    overflow: auto;
  }

  .filter__container__action {
    padding: 1rem;

    button {
      margin-right: 1rem;
    }
  }

  ${ theme.media.small } {
    height: ${ ( props ) => props.isVisible ? '80rem' : 0 };
  }
`;

const FilterItemContainer = styled.div`
  ${ ( props : FilterItemContainer ) => '' }
  display: flex;
  align-items: center;
  width: calc( ${ ( props ) => props.wide ? '100%' : '50%' } - 1.5rem );
  min-height: 7rem;

  margin: 1rem .75rem .5rem;
  padding: 1rem;

  background: ${ theme.colors.gray2 };
  border-radius: .5rem;

  > * {
    width: 100%;
  }

  ${ theme.media.small } {
    width: calc( 100% - 1.5rem );
  }
`;

export {
  FilterStyles,
  FilterContainerStyles,
  FilterInputStyles,
  FilterItemContainer,
};
