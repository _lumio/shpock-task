import styled from 'styled-components';
import theme from '../../../common/theme';

const FilterCheckboxStyles = styled.label`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  cursor: pointer;
  font-weight: bold;

  img {
    margin-right: .5rem;
  }

  input {
    visibility: hidden;
    position: absolute;
  }

  .filter-checkbox__label {
    width: 100%;
    padding-right: 1rem;
  }

  .filter-checkbox__checkbox {
    display: inline-flex;
    width: 5rem;
    height: 2.2rem;
    margin-right: .75rem;

    background: rgba( 255, 255, 255, 0 );
    border: 1px solid ${ theme.colors.gray1 };
    border-radius: 1.25rem;

    transition: background .25s;

    &::after {
      content: '';
      display: block;
      width: 2rem;
      height: 2rem;

      background: ${ theme.colors.white };
      border-radius: 1rem;
      box-shadow: .05rem .15rem .5rem rgba( 0, 0, 0, .25 );
      cursor: pointer;

      transition: transform .25s ease, background .25s;
    }
  }

  input:checked + .filter-checkbox__checkbox {
    &::after {
      background: ${ ( props ) => props.theme.colors.primary };
      transform: translateX( 2rem );
      transition: transform .25s ease, background .25s .15s;
    }
  }
`;

export { FilterCheckboxStyles };
