interface FilterCheckboxGraphQLType {
  id : string;
  label : string;
  icon : string;
  defaultValue : boolean;
}

interface FilterCheckboxPropsType {
  item : FilterCheckboxGraphQLType;
  value? : boolean;
  onChange( evt : any ) : void;
}

export {
  FilterCheckboxPropsType,
};
