import * as React from 'react';
import { FilterCheckboxPropsType } from './types';
import { FilterItemContainer } from '../styles';
import { FilterCheckboxStyles } from './styles';
import Icon from '../../Icon';

const FilterCheckbox : React.StatelessComponent<FilterCheckboxPropsType> = ( props : FilterCheckboxPropsType ) => {
  const { item } = props;
  const value = props.value || item.defaultValue;
  const onChange = ( evt : any ) => props.onChange( evt.target.checked );

  return (
    <FilterItemContainer>
      <FilterCheckboxStyles>
        <Icon icon={ item.icon } />
        <span className='filter-checkbox__label'>{ item.label }</span>
        <input type='checkbox' checked={ !!value } onChange={ onChange } />
        <span className='filter-checkbox__checkbox' />
      </FilterCheckboxStyles>
    </FilterItemContainer>
  );
};

export default FilterCheckbox;
