import * as React from 'react';
import { FilterRadioGroupPropsType } from './types';
import { FilterRadioGroupStyles } from './styles';
import { FilterItemContainer } from '../styles';
import FilterLabel from '../../FilterLabel';
import Icon from '../../Icon';

const FilterRadioGroup : React.StatelessComponent<FilterRadioGroupPropsType> = ( props : FilterRadioGroupPropsType ) => {
  const { item } = props;
  const value = props.value || item.defaultValue;
  const onChange = ( evt : any ) => props.onChange( evt.target.value );

  return (
    <FilterItemContainer wide>
      <FilterRadioGroupStyles>
        <FilterLabel icon={ item.icon }>{ item.label }</FilterLabel>
        <div className='filter-radio-group__options'>
          { item.options.map( ( option, index ) => (
            <label key={ index }>
              <input
                type='radio'
                value={ option.value }
                name={ item.id }
                onChange={ onChange }
                checked={ value === option.value }
              />
              <span className='filter-radio-group__radio' />
              <span className='filter-radio-group__label'>
                { option.label }
                { option.suffixIcon ? <Icon icon={ option.suffixIcon } /> : null }
              </span>
            </label>
          ) ) }
        </div>
      </FilterRadioGroupStyles>
    </FilterItemContainer>
  );
};

export default FilterRadioGroup;
