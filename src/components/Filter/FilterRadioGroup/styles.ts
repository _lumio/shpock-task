import styled from 'styled-components';
import theme from '../../../common/theme';

const FilterRadioGroupStyles = styled.div`
  .filter-radio-group__options {
    padding-top: .5rem;
    display: flex;
    justify-content: space-between;
    align-items: flex-start;
    flex-wrap: wrap;
  }

  label {
    width: 24%;
    display: inline-flex;
    justify-content: flex-start;
    align-items: center;
    margin-bottom: .5rem;
    padding: .75rem 1rem;

    color: ${ ( props : any ) => props.theme.colors.white };
    background: ${ ( props : any ) => props.theme.colors.primary };
    border-radius: .6rem;
    cursor: pointer;

    ${ theme.media.small } {
      width: 49%;
    }
  }

  input {
    visibility: hidden;
    position: absolute;
  }

  .filter-radio-group__radio {
    display: inline-flex;
    width: 1.5rem;
    height: 1.5rem;
    margin-right: .5rem;
    background: #fff;
    border: 2px solid ${ ( props : any ) => props.theme.colors.white };
    border-radius: .75rem;
    transition: background .25s;
  }

  input:checked + .filter-radio-group__radio {
    background: ${ ( props : any ) => props.theme.colors.primary };
  }

  .filter-radio-group__label {
    display: inline-flex;
    align-items: center;

    img {
      margin-left: .5rem;
    }
  }
`;

export { FilterRadioGroupStyles };
