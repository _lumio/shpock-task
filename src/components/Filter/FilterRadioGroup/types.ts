interface FilterRadioGroupOptionType {
  value : string | number;
  label : string;
  suffixIcon? : string;
}

interface FilterRadioGroupGraphQLType {
  id : string;
  label : string;
  icon : string;
  defaultValue : string | number;
  options : FilterRadioGroupOptionType[];
}

interface FilterRadioGroupPropsType {
  item : FilterRadioGroupGraphQLType;
  value? : string | number;
  onChange( evt : any ) : void;
}

export {
  FilterRadioGroupPropsType,
};
