import * as React from 'react';
import { FilterMinMaxPropsType } from './types';
import { FilterItemContainer } from '../styles';
import { FilterMinMaxStyles } from './styles';
import FilterLabel from '../../FilterLabel';

const getValue = ( val : number | string ) => {
  return val === undefined || val < 0 ? '' : val;
};

const FilterMinMax : React.StatelessComponent<FilterMinMaxPropsType> = ( props : FilterMinMaxPropsType ) => {
  let minDom : any;
  let maxDom : any;
  const { item } = props;

  const defaultValue = item.defaultValue || [];
  const value = props.value || [];
  const minVal = getValue( value[ 0 ] || defaultValue[ 0 ] );
  const maxVal = getValue( value[ 1 ] || defaultValue[ 1 ] );

  const onChange = ( evt : any ) => {
    const newMinVal = +( minDom.value || -1 );
    const newMaxVal = +( maxDom.value || -1 );

    props.onChange( [ newMinVal, newMaxVal ] );
  };

  return (
    <FilterItemContainer>
      <FilterMinMaxStyles>
        <FilterLabel icon={ item.icon }>{ item.label }</FilterLabel>
        <div className='filter-min-max__values'>
          <input
            type='number'
            ref={ ( dom ) => minDom = dom }
            placeholder={ item.minLabel }
            onChange={ onChange }
            value={ minVal }
          />
          <input
            type='number'
            ref={ ( dom ) => maxDom = dom }
            placeholder={ item.maxLabel }
            onChange={ onChange }
            value={ maxVal }
          />
        </div>
      </FilterMinMaxStyles>
    </FilterItemContainer>
  );
};

export default FilterMinMax;
