interface FilterMinMaxGraphQLType {
  id : string;
  label : string;
  icon : string;
  defaultValue : number[];
  minLabel : string;
  maxLabel : string;
}

interface FilterMinMaxPropsType {
  item : FilterMinMaxGraphQLType;
  value? : number[];
  onChange( evt : any ) : void;
}

export {
  FilterMinMaxPropsType,
};
