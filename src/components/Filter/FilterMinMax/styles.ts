import styled from 'styled-components';

const FilterMinMaxStyles = styled.div`
  .filter-min-max__values {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding-top: .25rem;

    input {
      width: calc( 50% - .25rem );

      font-size: 1.4rem;
      padding: .5rem;
      border: 1px solid ${ ( props : any ) => props.theme.colors.gray1 };
      border-radius: .5rem;
    }
  }
`;

export { FilterMinMaxStyles };
