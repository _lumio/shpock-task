import ValueType, { ValueCollectionType } from '../../common/valueType';

interface FilterGraphQLType {
  items : any[];
  placeholder : string;
}

interface FilterPropsType {
  filter : FilterGraphQLType;
  onChange?( values : { [ key : string ] : ValueType } ) : void;
}

interface FilterStateType {
  filtersAreVisible : boolean;
  values : ValueCollectionType;
}

interface FilterContainerStylesPropsType {
  isVisible? : boolean;
}

interface FilterItemContainer {
  wide? : boolean;
}

export {
  FilterPropsType,
  FilterStateType,
  FilterContainerStylesPropsType,
  FilterItemContainer,
};
