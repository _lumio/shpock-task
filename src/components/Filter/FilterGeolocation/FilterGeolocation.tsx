import * as React from 'react';
import { FilterGeolocationPropsType } from './types';
import { getCurrentLocation } from './helpers';
import { FilterItemContainer } from '../styles';
import { FilterGeolocationStyles } from './styles';
import FilterLabel from '../../FilterLabel';

// TODO: This probably needs to be a class as of right now,
// this component can change the state of an unmounted parent component (Filter)
// and will also override a users input.
const pendingLocation = 'location pending...';
const FilterGeolocation : React.StatelessComponent<FilterGeolocationPropsType> = ( props : FilterGeolocationPropsType ) => {
  let inputDom : any;
  const { item } = props;
  const value = props.value !== undefined ? props.value : item.defaultValue;
  let viewValue : string;
  let disabled = false;

  if ( value === true ) {
    viewValue = pendingLocation;
    disabled = true;
  }
  else {
    viewValue = String( value );
  }

  if ( value === true && props.filtersAreVisible ) {
    getCurrentLocation()
      .then( ( position ) => {
        props.onChange( position );
      } )
      .catch( () => {
        if ( inputDom && inputDom.value === pendingLocation ) {
          props.onChange( '' );
        }
      } );
  }

  const onChange = ( evt : any ) => props.onChange( evt.target.value );

  return (
    <FilterItemContainer>
      <FilterGeolocationStyles>
        <FilterLabel icon={ item.icon }>{ item.label }</FilterLabel>
        <input
          type='text'
          value={ viewValue }
          onChange={ onChange }
          ref={ ( dom ) => inputDom = dom }
          disabled={ disabled }
        />
      </FilterGeolocationStyles>
    </FilterItemContainer>
  );
};

export default FilterGeolocation;
