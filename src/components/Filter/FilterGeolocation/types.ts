interface FilterGeolocationCoords {
  latitude : number;
  longitude : number;
}

interface FilterGeolocationGraphQLType {
  id : string;
  label : string;
  icon : string;
  defaultValue : string | boolean;
}

interface FilterGeolocationPropsType {
  item : FilterGeolocationGraphQLType;
  value? : string;
  filtersAreVisible? : boolean;
  onChange( evt : any ) : void;
}

export {
  FilterGeolocationCoords,
  FilterGeolocationPropsType,
};
