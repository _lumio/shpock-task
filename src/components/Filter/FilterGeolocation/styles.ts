import styled from 'styled-components';

const FilterGeolocationStyles = styled.div`
  input {
    width: 100%;

    font-size: 1.4rem;
    padding: .5rem;
    border: 1px solid ${ ( props : any ) => props.theme.colors.gray1 };
    border-radius: .5rem;

    &:disabled {
      background: ${ ( props : any ) => props.theme.colors.gray2 };
    }
  }
`;

export { FilterGeolocationStyles };
