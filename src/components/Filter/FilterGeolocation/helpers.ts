import { FilterGeolocationCoords } from './types';

let coordsCache : FilterGeolocationCoords;
const getCurrentLocation = () => {
  if ( coordsCache ) {
    return Promise.resolve( `${ coordsCache.latitude },${ coordsCache.longitude }` );
  }

  if ( !( 'geolocation' in navigator ) ) {
    return Promise.reject( false );
  }

  return new Promise( ( resolve, reject ) => {
    navigator.geolocation.getCurrentPosition( ( position ) => {
      coordsCache = position.coords;
      resolve( `${ position.coords.latitude },${ position.coords.longitude }` );
    }, ( error ) => reject( false ) );
  } );
};

export { getCurrentLocation };
