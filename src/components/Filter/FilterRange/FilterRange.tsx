import * as React from 'react';
import { FilterRangePropsType } from './types';
import { FilterRangeStyles } from './styles';
import { FilterItemContainer } from '../styles';
import FilterLabel from '../../FilterLabel';
import Icon from '../../Icon';

const FilterRange : React.StatelessComponent<FilterRangePropsType> = ( props : FilterRangePropsType ) => {
  const { item } = props;
  const min = 0;
  const max = Math.max( props.item.steps.length - 1, 0 );
  const stepValues = item.steps.map( ( entry ) => entry.value );

  const value = props.value || item.defaultValue;
  const valueIndex = value ? stepValues.indexOf( value ) : 0;
  const onChange = ( evt : any ) => (
    props.onChange( stepValues[ evt.target.value ] )
  );

  return (
    <FilterItemContainer>
      <FilterRangeStyles>
        <FilterLabel icon={ item.icon } value={ item.steps[ valueIndex ].label }>
          { item.label }
        </FilterLabel>
        <div className='filter-range__range'>
          <Icon icon={ item.startIcon } />
          <input
            type='range'
            min={ min }
            max={ max }
            value={ valueIndex }
            onChange={ onChange }
            step={ 1 }
          />
          <Icon icon={ item.endIcon } />
        </div>
      </FilterRangeStyles>
    </FilterItemContainer>
  );
};

export default FilterRange;
