interface FilterRangeStepType {
  value : string | number;
  label : string;
}

interface FilterRangeGraphQLType {
  id : string;
  label : string;
  icon : string;
  startIcon : string;
  endIcon : string;
  defaultValue : string | number;
  steps : FilterRangeStepType[];
}

interface FilterRangePropsType {
  item : FilterRangeGraphQLType;
  value? : string | number;
  onChange( evt : any ) : void;
}

export {
  FilterRangePropsType,
};
