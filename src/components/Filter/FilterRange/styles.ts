import styled from 'styled-components';
import theme from '../../../common/theme';

const rangeThumb = ( css : string, base : string = '&' ) => (
  [
    '-webkit-slider-thumb',
    '-moz-range-thumb',
    '-ms-thumb',
  ].map( ( prefix ) => `
    ${ base }::${ prefix } {
      ${ css }
    }
  ` ).join( '\n' )
);

const rangeTrack = ( css : string, base : string = '&' ) => (
  [
    '-webkit-slider-runnable-track',
    '-moz-range-track',
    '-ms-track',
    '-ms-fill-lower',
    '-ms-fill-upper',
  ].map( ( prefix ) => ( `
    ${ base }::${ prefix } {
      ${ css }
    }
  ` ) ).join( '\n' )
);

const FilterRangeStyles = styled.div`
  .filter-range__range {
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-top: 1rem;

    img {
      &:first-child {
        margin-right: .5rem;
      }

      &:last-child {
        margin-left: .5rem;
      }
    }
  }

  input[ type=range ] {
    display: inline-flex;
    align-items: center;

    -webkit-appearance: none;
    width: 100%;
    background: transparent;

    &::-webkit-slider-thumb {
      -webkit-appearance: none;
    }

    ${ rangeThumb( `
      width: 2rem;
      height: 2rem;
      margin-top: -1rem;

      background: ${ theme.colors.white };
      border-radius: 1rem;
      box-shadow: .05rem .15rem .5rem rgba( 0, 0, 0, .25 );
      cursor: pointer;
    ` ) }

    &::-ms-track {
      width: 100%;
      cursor: pointer;

      background: transparent;
      border-color: transparent;
      color: transparent;
    }

    &::-ms-tooltip {
      display: none;
    }

    ${ rangeTrack( `
      width: 100%;
      height: 1px;
      cursor: pointer;
      box-shadow: none;
      background: ${ theme.colors.primary };
      border: none;
    ` ) }

    &:focus {
      outline: none;
    }
  }
`;

export { FilterRangeStyles };
