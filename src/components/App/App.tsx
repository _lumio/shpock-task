import * as React from 'react';
import PageHeader from '../PageHeader';
import { AppPropsType, AppStateType } from './types';
import { ValueCollectionType } from '../../common/valueType';

class App extends React.Component<AppPropsType, AppStateType> {
  constructor( props : AppPropsType ) {
    super( props );
    this.state = {
      values: {},
    };
  }

  changeValues = ( values : ValueCollectionType ) => {
    // tslint:disable-next-line
    console.log( values );
    this.setState( { values } );
  }

  render() {
    return (
      <>
        <PageHeader
          pageHeader={ this.props.page.pageHeader }
          onFilterChange={ this.changeValues }
        />
        <pre>{ JSON.stringify( this.state.values, null, 2 ) }</pre>
      </>
    );
  }
}

export default App;
