import { ValueCollectionType } from '../../common/valueType';

interface AppPropsType {
  page : any;
}

interface AppStateType {
  values : ValueCollectionType;
}

export { AppPropsType, AppStateType };
