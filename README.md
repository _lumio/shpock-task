shpock task
===========

This project uses React with styled-components to form a simple
search/filter interface.

The filters are shown as soon as the user starts typing into the search field.

start
-----

You need NodeJS with npm or yarn on your machine to run this project.
To start it use the following commands:

```bash
yarn && yarn start # for yarn users
npm i && npm start # for npm users
pnpm i && pnpm start # for pnpm users
```
